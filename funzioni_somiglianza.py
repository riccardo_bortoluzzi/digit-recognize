'''
File che contiene le funzioni per stabilire quanto 2 immagini risutino simili
Le funzioni qui presenti che verranno richiamate all'esterno del modulo  
dovranno avere come parametri minimi:
lista_pixel_test: lista dei valori dei pixel dell'immagine di test
lista_pixel_train: lista dei valori dei pixel dell'immagine di train
'''

import math
import numpy as np

def controlla_pixel_intorno(pixel, lista_pixel, lato):
    '''
    Funzione per verificare se il rispettivo pixel si trova all'interno di un intorno di raggio r di uno di quelli nella lista
    
    pixel: tupla di 2 elementi del tipo (r,c)
    lista_pixel: lista di tuple di 2 elementi del tipo pixel
    lato: numero che rappresenta il lato del quadrato

    Return: Booleano che indica se un determinato pixel e contenuto in un intorno del pixel della lista passata in input
    '''
    #estraggo i valori di r e c del pixel in questione
    (r_p, c_p) = pixel
    #poiche lato sara sicuramente dispari per come costruita la funzione calcolo il mezzo lato
    mezzo_lato = (lato-1)/2
    for (r_l,c_l) in lista_pixel:
        if ((r_l-mezzo_lato) <= r_p <= (r_l+mezzo_lato)) and ((c_l-mezzo_lato) <= c_p <= (c_l+mezzo_lato)):
            return True
    #se arrivo qui ho scorso tutti i pixel e non ho trovato nessuno che disti da quello assegnato meno di mezzo_lato, ritorno false
    return False

def punteggio_intorni_train(lista_pixel_test, lista_pixel_train, solo_neri = False):
    '''
    Funzione per calcolare il punteggio dell'immagine di test rispetto a quella di training
    il punteggio e' pari al minimo raggio tale che 
    l'immagine di test sia contenuta nell'unione di tutti i quadrati di lato raggio centrati nei pixel colorati dell'imamgine di train
    
    lista_pixel_test : lista di numeri interi che rappresentano i pixel colorati dell'immagine di test
    lista_pixel_train : lista di numeri interi che rappresentano i pixel colorati dell'immagine di train 
    solo_neri: flag che indica se considerare solo i pixel completamente neri (255) oppure anche gli altri (!=0)

    return: return: numero che rappresenta il minimo lato in modo che l'immagine di test sia contenuta nell'intorno di lato l dell'imamgine di train
    '''
            
    k_dim_immagine = 28 #numero che rappresenta la dimensione dell'immagine ipotizzandola quadrata
    #valore soglia per considerare il tratto nero
    k_valore_soglia = 250
    #inizializzo le liste che rapresentano i pixel colorati di ciascuna immagine
    lista_pixel_col_test = []
    lista_pixel_col_trial = []
    if solo_neri:
        for r in range(k_dim_immagine):
            for c in range(k_dim_immagine):
                #controllo se l'immagine di test risulta colorata nel pixel individuato
                if lista_pixel_test[ r*k_dim_immagine + c ] >= k_valore_soglia:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_test.append( (r,c) )
                #controllo se il pixel nell'immagine di trial risulta colorato
                if lista_pixel_train[ r*k_dim_immagine + c ] >= k_valore_soglia:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_trial.append( (r,c) )
    else:
        #considero tutti i pixel colorati
        for r in range(k_dim_immagine):
            for c in range(k_dim_immagine):
                #controllo se l'immagine di test risulta colorata nel pixel individuato
                if lista_pixel_test[ r*k_dim_immagine + c ] != 0:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_test.append( (r,c) )
                #controllo se il pixel nell'immagine di trial risulta colorato
                if lista_pixel_train[ r*k_dim_immagine + c ] != 0:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_trial.append( (r,c) )
    #ora le liste dovrebbero avere tutti e soli i pixel colorati 
    #se lista_pixel_col_test e vuota va in errore significa che bisogna dimuire il livello di nero
    if lista_pixel_col_test == []:
        if solo_neri == True:
            raise ValueError('Attenzione!! Valore soglia nero troppo alto. Valore corrente: %d'%(k_valore_soglia))
        else:
            #si prendono tutti i colori, l'immagine fornita e bianca
            raise ValueError('Attenzione!! Immagine di test completamente bianca')
    #inizializzo il raggio
    r = 1
    while r<k_dim_immagine:
        #itero al massimo fino a quando il raggio e pari alla dimensione dell'imamgine, a quel punto ritornerei il numero stesso
        #creo un flag che indica se l'imamgine trial e contenuta nell'intorno dell'immaagine test
        flag_contenuta = True
        for px in lista_pixel_col_test:
            #itero per tutti i pixel della lista di test da controllare
            if controlla_pixel_intorno(px, lista_pixel_col_trial, r) == False:
                #il pixel non e contenuto in nessun intorno
                flag_contenuta = False
                #interrompo il primo ciclo for perche so che devo aumentare il lato
                break   
        #controllo se e contenuto
        if flag_contenuta:
            return r
        else:
            #se non e contentua aumento il raggio
            #aumento di 2 in modod da renderlo sempre dispari
            r+=2
    return r


def punteggio_intorni_test(lista_pixel_test, lista_pixel_train, solo_neri = False):
    '''
    Funzione per calcolare il punteggio dell'immagine di test rispetto a quella di training
    il punteggio e' pari al minimo raggio tale che 
    l'immagine di train sia contenuta nell'unione di tutti i quadrati di lato raggio centrati nei pixel colorati dell'imamgine di test
    
    lista_pixel_test : lista di numeri interi che rappresentano i pixel colorati dell'immagine di test
    lista_pixel_train : lista di numeri interi che rappresentano i pixel colorati dell'immagine di train 
    solo_neri: flag che indica se considerare solo i pixel completamente neri (255) oppure anche gli altri (!=0)

    return: numero che rappresenta il minimo lato in modo che tutte l'immagine di train sia contenute nell'intorno di lato l dell'immagine di test
    '''
            
    k_dim_immagine = 28 #numero che rappresenta la dimensione dell'immagine ipotizzandola quadrata
    #valore soglia per considerare il tratto nero
    k_valore_soglia = 250
    #inizializzo le liste che rapresentano i pixel colorati di ciascuna immagine
    lista_pixel_col_test = []
    lista_pixel_col_trial = []
    if solo_neri:
        for r in range(k_dim_immagine):
            for c in range(k_dim_immagine):
                #controllo se l'immagine di test risulta colorata nel pixel individuato
                if lista_pixel_test[ r*k_dim_immagine + c ] >= k_valore_soglia:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_test.append( (r,c) )
                #controllo se il pixel nell'immagine di trial risulta colorato
                if lista_pixel_train[ r*k_dim_immagine + c ] >= k_valore_soglia:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_trial.append( (r,c) )
    else:
        #considero tutti i pixel colorati
        for r in range(k_dim_immagine):
            for c in range(k_dim_immagine):
                #controllo se l'immagine di test risulta colorata nel pixel individuato
                if lista_pixel_test[ r*k_dim_immagine + c ] != 0:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_test.append( (r,c) )
                #controllo se il pixel nell'immagine di trial risulta colorato
                if lista_pixel_train[ r*k_dim_immagine + c ] != 0:
                    #aggiungo il pixel alla rispettiva lista
                    lista_pixel_col_trial.append( (r,c) )
    #ora le liste dovrebbero avere tutti e soli i pixel colorati 
    #se lista_pixel_col_test e vuota va in errore significa che bisogna dimuire il livello di nero
    if lista_pixel_col_test == []:
        if solo_neri == True:
            raise ValueError('Attenzione!! Valore soglia nero troppo alto. Valore corrente: %d'%(k_valore_soglia))
        else:
            #si prendono tutti i colori, l'immagine fornita e bianca
            raise ValueError('Attenzione!! Immagine di test completamente bianca')
    #inizializzo il raggio
    r = 1
    while r<k_dim_immagine:
        #itero al massimo fino a quando il raggio e pari alla dimensione dell'imamgine, a quel punto ritornerei il numero stesso
        #creo un flag che indica se l'imamgine trial e contenuta nell'intorno dell'immaagine test
        flag_contenuta = True
        for px in lista_pixel_col_trial:
            #itero per tutti i pixel della lista di train da controllare
            if controlla_pixel_intorno(px, lista_pixel_col_test, r) == False:
                #il pixel non e contenuto in nessun intorno
                flag_contenuta = False
                #interrompo il primo ciclo for perche so che devo aumentare il lato
                break   
        #controllo se e contenuto
        if flag_contenuta:
            return r
        else:
            #se non e contentua aumento il raggio
            #aumento di 2 in modod da renderlo sempre dispari
            r+=2
    return r


def punteggio_quadrettatura(lista_pixel_test, lista_pixel_train, pixel_lato, soglia = 0):
    '''
    Funzione per assegnare un punteggio di somiglianza delle imamgini 
    suddividendole in quadrati piu piccoli e 
    contando quanti pixel colorati ci sono in ogni quadrato
    
    lista_pixel_test : lista di numeri interi che rappresentano i pixel colorati dell'immagine di test
    lista_pixel_train : lista di numeri interi che rappresentano i pixel colorati dell'immagine di train 
    pixel_lato: numero naturale che rappresenta il lato del quadrato della suddivisione
                utilizzare solo divisori esatti della dimensione dell'immagine (28)
    soglia: valore soglia per cui considerare un pixel colorato o meno 
            (es 0 tutti i pixel non bianchi, 250 i pixel quasi completamente neri)
            
    return: un numero intero che rappresenta la somma  delle differenze tra il numero di pixel colorati 
            per ciascuna imamgine nei singoli quadrati
    '''
    k_dim_imm = 28 #lato in pixel dell'imamgine
    #se lato_quadrato non e divisore perfetto della dimensione dell'imamgine ritorno un errore
    if k_dim_imm%pixel_lato !=0:
        raise ValueError('Attenzione suddivisione in quadrettatura non consentita')
    #calcolo quanti quadrati ci andranno per ogni lato
    quad_x_lato = int(k_dim_imm/pixel_lato)
    #creo le matrici che rappresentano il numero di pixel colorati per ogni imamgine
    n_pixel_col_test = np.array( [[0]*quad_x_lato]*quad_x_lato )
    n_pixel_col_train = np.array( [[0]*quad_x_lato]*quad_x_lato )
    #itero per tutti i pixel possibili
    for r in range(k_dim_imm):
        for c in range(k_dim_imm):
            #il pixel di riferimento sara r*k_dim_imm + c
            index_pixel = r*k_dim_imm + c
            #verifico se il pixel del test e colorato secondo la soglia
            if lista_pixel_test[index_pixel] > soglia:
                #e colorato secondo la soglia -> aumento di 1 il suo contatore
                n_pixel_col_test[ int(r/pixel_lato), int(c/pixel_lato) ] += 1
            #verifico se il pixel del train e colorato secondo la soglia
            if lista_pixel_train[index_pixel] > soglia:
                #e colorato secondo la soglia -> aumento di 1 il suo contatore
                n_pixel_col_train[ int(r/pixel_lato), int(c/pixel_lato) ] += 1
    #ora le due matrici sono popolate con il numero di pixel colorati in ciascun quadrato
    #calcolo la matrice differenza
    diff = abs(n_pixel_col_test - n_pixel_col_train)
    #print('Matride differenza')
    #print(diff)
    #il punteggio e dato dalla somma di tutti valori di diff
    #print('Punteggio quadrettatura: ', np.sum(diff))
    return np.sum(diff)
            

#Funzioni per il calcolo della somiglianza attraverso la funzione di traslazione

# def trasla(matrice, orizz, vert):
#     '''
#     Funzione per traslare una array di numpy
#     cirea una matrice piu grande e poi mostra solo la porzione che mi interessa
#     
#     codice ispirato da https://stackoverflow.com/questions/44874512/how-to-translate-shift-a-numpy-array
#     
#     matrice: matrice del tipo numpy.array che rappresenta un'immagine
#     orizz: vettore orizzontale per traslare
#     vert: vettore verticale per traslare
#     
#     return: una matrice traslata
#     '''
#     (n,m)=matrice.shape
#     espansione=np.zeros((2*n,2*m)) 
#     espansione[n:2*n,m:2*m]= matrice
#     vett_v = n-vert
#     vett_orizz = m-orizz
#     return espansione[vett_v: vett_v+n, vett_orizz: vett_orizz+m]
#     
def trova_minimo_traslazione(matrice_ferma, matrice_da_traslare, funzione_confronto, soglia=0):
    '''
    Funzione per ritornare il minimo punteggio raggiunto dalle 2 immagini sotto traslazione
    La traslazione massima e calcolata in moddo che tutta l'immagine possa essere all'interno di entrambe le immagini
    praticamente viene calcolato il bordo destro e basso dell'imamgine da traslare
    Le matrici devono essere della stessa dimensione e quadrate
    Non si considera mai la traslazione nulla
    
    matrice_ferma: array di numpy di cui non si eseguira la traslazione
    matrice_da_traslare: matrice di cui si eseguira la traslazione
    funzione_confronto: prende in input 2 matrici e ritorna un numero che rappresenta quanto esse siano simili
    soglia: valore che indica la soglia minima per considerare un pixel colorato
            (serve per determinare il vettore massimo di traslazione)
    '''
    #calcolo la matrice soglia
    (dim_v,dim_o)=matrice_da_traslare.shape
    #dim_v e dim_o devono avere lo stesso valore
    if dim_v != dim_o:
        raise ValueError('Le matrici nella funzione trova_minimo_traslazione devono avere la stessa dimensione')
    matrice_soglia = np.array([soglia] *dim_v )
    #calcolo il valore del bordo destro
    bordo_destro = 0
    while False not in (matrice_da_traslare[:, -bordo_destro -1] < matrice_soglia):
        bordo_destro += 1 
    #calcolo il bordo in basso
    bordo_basso = 0
    while False not in (matrice_da_traslare[-bordo_basso-1,:] < matrice_soglia):
        bordo_basso += 1
    #creo la matrice da cui andro a prendere gli ementi di traslazione
    espansione=np.zeros((2*dim_v,2*dim_o)) 
    espansione[dim_v:2*dim_v,dim_o:2*dim_o]= matrice_da_traslare
    #inizializzo il valore minimo
    valore_minimo = math.inf
    #comincio le traslazioni
    for v_vert in range(bordo_basso):
        #escludo la translazione nulla
        if v_vert == 0:
            v_orizz_min = 1
        else:
            v_orizz_min = 0
        for v_oriz in range(v_orizz_min, bordo_destro):
            #nuovo_valore = funzione_confronto(matrice_ferma, trasla(matrice_da_traslare, v_oriz, v_vert) )
            nuovo_valore = funzione_confronto(matrice_ferma, espansione[dim_v-v_vert: 2*dim_v - v_vert, dim_o-v_oriz: 2*dim_o - v_oriz])
            #verifico se e minore di quella attuale
            if nuovo_valore < valore_minimo:
                valore_minimo = nuovo_valore
    return valore_minimo

#funzioni per calcolare il valore di similiturde tra 2 matrici

def sum_diff_pixel(matrice1, matrice2):
    '''
    Funzioen per ritornare la somma delle differenze dei pixel tra 2 matrici
    
    matrice1: prima matrice da confrontare
    matrice2: seconda matrice da confrontare
    
    return: numero
    '''
    #calcolo la differenza
    diff = abs(matrice1 - matrice2)
    #ritorno la somam degli elementi
    return np.sum(diff)

def num_pixel_diversi(matrice1, matrice2, soglia):
    '''
    Funzione per caclolare il numero di pixel colorati diversi 
    i pixel sono presi come colorati se maggiori di soglia, bianchi se minori della soglia
    
    matrice1: prima matrice da confrontare
    matrice2: seconda matrice da confrontare
    soglia: numero che indica quando considerare un pixel colorato e quando considerarlo bianco
    
    return: numero intero che rappresenta quanti pixel colorati sono diversi
    '''
    #inizializzo il contatore
    contatore = 0
    '''
    #calcolo le dimensioni delle amtrici
    (dim_v, dim_o) = matrice1.shape
    #itero per le righe
    for r in range(dim_v):
        #itero per tutte le colonne
        for c in range(dim_o):
            #verifico se la prima matrice risulta colorata e la seconda no
            if (matrice1[r,c]> soglia) and (matrice2[r,c] < soglia) :
                #incremento il contatore
                contatore += 1
            #verifico se la prima matrice risulta bianca e la seconda colorata
            elif (matrice1[r,c] < soglia) and (matrice2[r,c] > soglia) :
                #incremento il contatore
                contatore += 1
    '''
    #verifico se la prima matrice risulta colorata e la seconda no
    col1 = (matrice1 >= soglia) 
    bia2 = (matrice2 < soglia)
    control1 = col1*bia2
    #incremento il contatore
    contatore += np.count_nonzero(control1 == 1)
    #verifico se la prima matrice risulta bianca e la seconda no
    bia1 = (matrice1 < soglia) 
    col2 = (matrice2 >= soglia)
    control2 = bia1*col2
    #incremento il contatore
    contatore += np.count_nonzero(control2 == 1)
    #print('m1: ', matrice1, 'm2: ', matrice2, 'contatore: ', contatore) 
    #ritorno il contatore
    return contatore

#funzione di traslazione generale

def blockshaped(arr, nrows):
    """
    Return an array of shape (n, nrows, ncols) where
    n * nrows * ncols = arr.size

    If arr is a 2D array, the returned array should look like n subblocks with
    each subblock preserving the "physical" layout of arr.
    
    codice preso da https://stackoverflow.com/questions/16856788/slice-2d-array-into-smaller-2d-arrays
    """
    h, w = arr.shape
    #assert h % nrows == 0, "{} rows is not evenly divisble by {}".format(h, nrows)
    #assert w % ncols == 0, "{} cols is not evenly divisble by {}".format(w, ncols)
    return (arr.reshape(h//nrows, nrows, -1, nrows)
               .swapaxes(1,2)
               .reshape(-1, nrows, nrows))

def diff_quadrett_px_col_perc(matrice1, matrice2, soglia, lato_quadrato):
    '''
    Inizialmente si dividono le imamgini in quadrati piu piccoli.
    In ogni quadrato conta i pixel colorati e ne fa la percentuale rispetto all'intera imamgine.
    ritorna la somma delle differenze di queste percentuali
    
    matrice1: matrice che indica la prima imamgine
    matrice2: matrice della seconda imamgine
    soglia: valore che indica quando considerare un pixel colorato
    lato_quadrato: lato del quadrato piu piccolo in cui dividere l'immagine
                   (deve essere un divisore intero della dimensione delle matrici)
                   
    return: numero che rappresenta la somma dei valori assoluti delle differenze delle percentuali
    '''
    #calcolo le dimensioni delle imamgini
    (dim_v, dim_o) = matrice1.shape
    #verifico se lato_quadrato e un divisore intero delle dimensioni
    if dim_v%lato_quadrato != 0:
        raise ValueError("Il lato del quadrato deve essere un divisore della dimensione dell'immagine")
    #creo le matrici che indicano i pixel colorati
    m1_col = matrice1 >= soglia
    m2_col = matrice2 >= soglia
    #calcolo i pixel colorati per ciascuna immagine
    n_px_col_m1 = np.sum(m1_col)
    n_px_col_m2 = np.sum(m2_col)
    #inizializzo il punteggio
    punteggio = 0
    #calcolo la suddivisione delle 2 matrici
    sudd_1 = blockshaped(m1_col, lato_quadrato)
    sudd_2 = blockshaped(m2_col, lato_quadrato)
    #itero per tutti i quadrati
    '''
    for r in range(0, dim_v, lato_quadrato):
        for c in range(0, dim_o, lato_quadrato):
    '''
    for i in range(len(sudd_1)): 
        #calcolo i pixel colorati nella prima imamgine
        px_col_qua_m1 = np.sum(sudd_1[i])
        #calcolo la percentuale nella prima immagine
        perc1 = px_col_qua_m1 / n_px_col_m1
        #calcolo i pixel colorati nella seconda imamgine
        px_col_qua_m2 = np.sum(sudd_2[i])
        #calcolo la percentuale nella seconda immagine
        perc2 = px_col_qua_m2 / n_px_col_m2
        #incremento il punteggio della differenza delle 2 percentuali
        punteggio += abs( perc1 - perc2 )
    #ritorno il punteggio calcolato
    return punteggio

def diff_quadrett_inchiostro_perc(matrice1, matrice2, lato_quadrato):
    '''
    Inizialmente si dividono le imamgini in quadrati piu piccoli.
    In ogni quadrato conta la quantita di inchiostro utilizzato (come somma dei valori dei pixel)
    e ne fa la percentuale rispetto all'intera imamgine.
    ritorna la somma delle differenze di queste percentuali
    
    matrice1: matrice che indica la prima imamgine
    matrice2: matrice della seconda imamgine
    lato_quadrato: lato del quadrato piu piccolo in cui dividere l'immagine
                   (deve essere un divisore intero della dimensione delle matrici)
                   
    return: numero che rappresenta la somma dei valori assoluti delle differenze delle percentuali
    '''
    #calcolo le dimensioni delle imamgini
    (dim_v, dim_o) = matrice1.shape
    #verifico se lato_quadrato e un divisore intero delle dimensioni
    if dim_v%lato_quadrato != 0:
        raise ValueError("Il lato del quadrato deve essere un divisore della dimensione dell'immagine")
    #calcolo la quantita di inchiostro utilizzata per ciascuna immagine
    inch_1 = np.sum(matrice1)
    inch_2 = np.sum(matrice2)
    #inizializzo il punteggio
    punteggio = 0
    #calcolo la suddivisione delle 2 matrici
    sudd_1 = blockshaped(matrice1, lato_quadrato)
    sudd_2 = blockshaped(matrice2, lato_quadrato)
    #itero per tutti i quadrati
    '''
    for r in range(0, dim_v, lato_quadrato):
        for c in range(0, dim_o, lato_quadrato):
    '''
    for i in range(len(sudd_1)):
        #calcolo la quantita di inchiostro nella prima imamgine
        inch_qua_m1 = np.sum(sudd_1[i] )
        #calcolo la percentuale nella prima immagine
        perc1 = inch_qua_m1 / inch_1
        #calcolo la quantita di inchiostro nella seconda imamgine
        inch_qua_m2 = np.sum(sudd_2[i])
        #calcolo la percentuale nella seconda immagine
        perc2 = inch_qua_m2 / inch_2
        #incremento il punteggio della differenza delle 2 percentuali
        punteggio += abs( perc1 - perc2 )
    #ritorno il punteggio calcolato
    return punteggio

def confr_perc_px_px(matrice1, matrice2):
    '''
    funzione per eseguire il confronto percentuale pixel per pixel del colore utilizzato
    
    matrice1: matrice che indica la prima imamgine
    matrice2: matrice della seconda imamgine
    
    return: numero
    '''
    #calcolo la quantita di inchiostro utilizzata per ciascuna immagine
    inch_1 = np.sum(matrice1)
    inch_2 = np.sum(matrice2)
    #inizializzo il punteggio
    punteggio = 0
    #calcolo le percentuali di colore
    perc_1 = matrice1 / inch_1
    perc_2 = matrice2 / inch_2
    punteggio = np.sum(abs (perc_1 - perc_2) )
    return punteggio   

def distanza_euclidea(matrice1, matrice2):
    '''
    funzione per calcolare la distanza euclidea tra 2 matrici
    
    matrice1: matrice che indica la prima imamgine
    matrice2: matrice della seconda imamgine
    
    return: numero che indica la distanza euclidea tra le 2 matrici
    '''
    #calcolo la differenza tra le 2 matrici al quadrato
    diff = (matrice1 - matrice2) **2
    #ritorno la radice quadrata della somma
    return math.sqrt( np.sum(diff))

def punteggio_minimo_matriciale(lista_pixel_test, lista_pixel_train, funzione_confronto_matrici, traslazione = False, soglia = 0):
    '''
    Funzione per assegnare un punteggio di somiglianza delle imamgini 
    traslandole (eventualmente) e ritornando il minor valore calcolato
    
    lista_pixel_test : lista di numeri interi che rappresentano i pixel colorati dell'immagine di test
    lista_pixel_train : lista di numeri interi che rappresentano i pixel colorati dell'immagine di train 
    funzione_confronto_matrici: funzione che prende in input 2 matrici e ritorna un punteggio 
                                (piu basso e il punteggio piu le matrici sono simili)
    traslazione: booleano che indica se eseguire le traslazioni delle immagini o meno
    soglia: valore che indica la soglia minima per considerare un pixel colorato
            (serve per determinare il vettore massimo di traslazione)
            
    return: un numero intero che rappresenta la somma  dei punteggi assegnati
            nella migliore traslazione
    '''
    #inizializzo la dimensione dell'immagine
    k_dim_imm = 28    
    #calcolo la matrice dell'immagine di test
    matrice_imm_test = np.array( [lista_pixel_test[i*k_dim_imm: (i+1)*k_dim_imm] for i in range(k_dim_imm) ] )
    #calcolo la matrice dell'imamgine di train
    matrice_imm_train = np.array( [lista_pixel_train[i*k_dim_imm: (i+1)*k_dim_imm] for i in range(k_dim_imm) ] )
    #calcolo il valore iniziale come la differenza a traslazione 0
    punteggio_attuale = funzione_confronto_matrici(matrice_imm_test, matrice_imm_train)
    if traslazione == True:
        #applico il divide et impera
        #ora faccio traslare l'imamgine di test
        nuovo_punteggio = trova_minimo_traslazione(matrice_imm_train, matrice_imm_test, funzione_confronto_matrici, soglia)
        #controllo se e piu piccolo
        if nuovo_punteggio < punteggio_attuale:
            punteggio_attuale = nuovo_punteggio
        #ora faccio traslare l'imamgine di train
        nuovo_punteggio = trova_minimo_traslazione(matrice_imm_test, matrice_imm_train, funzione_confronto_matrici, soglia)
        #controllo se e piu piccolo
        if nuovo_punteggio < punteggio_attuale:
            punteggio_attuale = nuovo_punteggio
    #ritorno il piu piccolo punteggio calcolato fino adesso
    return punteggio_attuale

def punteggio_minimo_no_traslazione(lista_pixel_test, lista_pixel_train, funzione_confronto_matrici):
    '''
    Funzione per assegnare un punteggio di somiglianza delle imamgini 
    utilizzando la funzione passata in input
    
    lista_pixel_test : lista di numeri interi che rappresentano i pixel colorati dell'immagine di test
    lista_pixel_train : lista di numeri interi che rappresentano i pixel colorati dell'immagine di train 
    funzione_confronto_matrici: funzione che prende in input 2 matrici e ritorna un punteggio 
                                (piu basso e il punteggio piu le matrici sono simili)
            
    return: un numero intero che rappresenta la somma dei punteggi assegnati
    '''
    #inizializzo la dimensione dell'immagine
    k_dim_imm = 28    
    #calcolo la matrice dell'immagine di test
    matrice_imm_test = np.array( [lista_pixel_test[i*k_dim_imm: (i+1)*k_dim_imm] for i in range(k_dim_imm) ] )
    #calcolo la matrice dell'imamgine di train
    matrice_imm_train = np.array( [lista_pixel_train[i*k_dim_imm: (i+1)*k_dim_imm] for i in range(k_dim_imm) ] )
    #calcolo il valore iniziale come la differenza a traslazione 0
    punteggio_attuale = funzione_confronto_matrici(matrice_imm_test, matrice_imm_train)
    #ritorno il punteggio calcolato
    return punteggio_attuale

def punteggio_categoria_media_n_px_col(lista_pixel_test, lista_pixel_train, soglia =0):
    '''
    Funzione per calcolare la probabilita che un'immagine di test
    sia simile a una data
    
    lista_pixel_test : lista di numeri interi che rappresentano i pixel colorati dell'immagine di test
    lista_pixel_train : lista di numeri che rappresentano la percentuale di pixel colorati dell'immagine di train
    soglia: numero intero per cui considerare un pixel colorato o meno
    
    return: un numero <1 che rappresenta la probabilita che l'immagine sia simile a quella data 
    '''
    #inizializzo gli array delle imamgini
    immagine_test = np.array(lista_pixel_test)
    prob_immagine_train = np.array(lista_pixel_train)
    #controllo i pixel colorati dell'imamgine di test
    #utilizzo l'1* per convertirla in numeri interi
    px_col_imm_test = 1* (immagine_test>+soglia)
    #calcolo la probabilita in base ai pixel colorati
    prob_px_col = px_col_imm_test * prob_immagine_train
    #calcolo la probabilita in base ai pixel non colorati
    prob_px_no_col = (1-px_col_imm_test) * (1-prob_immagine_train)
    #calcolo la probabilita toale come somma delle matrici precedenti
    prob_tot = prob_px_col + prob_px_no_col
    #calcolo il prodotto tra tutte queste probabilita
    return np.prod(prob_tot)


Script per il riconoscimento di cifre manoscritte

Il progetto è nato come approfondimento personale di un corso di 
apprendimento automatico della Laurea Magistrale in Informatica presso 
l'Università di Padova.

Il dataset è stato preso dalla competizione di kaggle presente al seguente link: 
https://www.kaggle.com/c/digit-recognizer.

Il software è da vedere come un primo approccio all'intelligenza artificiale
e al machine learning. 
Escludendo le funzioni di cui è segnata la fonte, il codice è stato scritto 
da zero, senza prendere spunto da progetti o script già esistenti. 
Gli algoritmi presenti sono frutto di riflessioni personali 
avvenute in seguito alla spiegazione in aula della logica dell'algoritmo knn.

Le problematiche che si sono dovute affrontare sono le seguenti:

1) Implementazione di un algoritmo che, date 2 immagini, ritorni un numero 
   indice di quanto gli input siano simili tra loro 
   (file funzioni_somiglianza.py);

2) implementazione di una procedura che, data un'immagine di test e 
   calcolati i rispettivi punteggi con tutte le immagini di train, 
   restituisca la categoria più probabile a cui appartenga l'input
   (file funzioni_approssimazioni.py).

Per lanciare il software e' necessario python 3.
Eseguire il seguente comando:
python3 script_digit_recognizer.py

La migliore percentuale di riconsocimento finora testata
(basandosi sulle prime circa 1800 classificazioni) e' di poco susperiore al 90%.
       

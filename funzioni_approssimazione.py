'''
File che contiene le funzioni per ritornare 
la classe di migliore approssimazione dell'immagine.
Le funzioni qui presenti che verranno passate come argomento 
del parametro p_funzione_migliore_abbinamento della funzione 
main.suddividi_immagini_generale
dovranno avere come parametri minimi:
lista_pixel_test: lista dei valori dei pixel dell'immagine di test
file_train: path del file contenente le imamgini di train da leggere
'''

import os, math
import numpy as np

def punteggio_migliore_massimo(diz_punteggi):
    '''
    Funzione per ritornare la chiave che corrisponde al massimo punteggio
    
    diz_punteggi e' un dizionario che ha come chiavi i numeri in stringa rappresentati e come valori i punteggi raggiunti

    return: chiave del dizionario con valore piu alto
    '''
    max_punteggio = -math.inf #inizializzo a - infinito
    for k in diz_punteggi.keys():
        punteggio_chiave = diz_punteggi[k]
        #verifico se il massimo viene raggiunto con disuguaglianza stretta
        if punteggio_chiave > max_punteggio:
            max_punteggio = punteggio_chiave
            migliore_chiave = k
    return migliore_chiave #dovrebbe essere sempre valorizzato se il dizionario di punteggi e popolato

def punteggio_migliore_minimo(diz_punteggi):
    '''
    Funzione per ritornare la chiave che corrisponde al minimo punteggio
    
    diz_punteggi e' un dizionario che ha come chiavi i numeri in stringa rappresentati e come valori i punteggi raggiunti

    return: chiave del dizionario con valore piu' piccolo
    '''
    min_punteggio = math.inf #inizializzo a - infinito
    for k in diz_punteggi.keys():
        punteggio_chiave = diz_punteggi[k]
        #verifico se il massimo viene raggiunto con disuguaglianza stretta
        if punteggio_chiave < min_punteggio:
            min_punteggio = punteggio_chiave
            migliore_chiave = k
    return migliore_chiave #dovrebbe essere sempre valorizzato se il dizionario di punteggi e popolato

def trova_elemento_piu_percentuale(diz_elementi, diz_contatori, ultimo_passaggio = False):
    '''
    Funzione per ritornare la chiave che compare piu in percentuale maggiore
    se 2 chiavi hanno entrambe la percentuale 100% ritorna '-1'
    
    diz_elementi: dizionario che rappresenta quanti elementi ci sono nell'insieme considerato di ogni titpo
    diz_contatori : dizionario che rappresenta quanti elementi totatli ci sono per ogni tipo
    ultimo_passaggio: flag che indica se ritornare comunque un elemento oppure ritornare -1 se ci sono piu elementi con percentuale 100%
    return: la chiave, se presente, nel dizionario con percentuale maggiore, oppure la stringa '-1'
    '''
    #inizializzo la variabile che conterra la massima percentuale
    massima_perc = -math.inf
    #itero sulle chiavi
    for k in diz_elementi.keys():
        #calcolo la nuova percentuale
        nuova_perc = diz_elementi[k]/diz_contatori[k]
        #controllo se e maggiore
        if nuova_perc > massima_perc:
            massima_perc = nuova_perc
            massima_chiave = k
        #controllo se entrambe sono pari a 1 cioie al massimo
        elif nuova_perc == massima_perc and nuova_perc == 1 and ultimo_passaggio==False:
            #ritorna l'errore '-1'
            return '-1'
    #al termine ritorna la migliore chiave
    return massima_chiave

def approssimazione_media_generale(lista_pixel_test, file_train, funzione_punteggio, funzione_migliore_punteggio):
    '''
    Funzione generale per calcolare la migliore approssimazione data l'immagine di test basandosi su un conto di media
    La funzione confronta l'immagine di test con ogni immagine di train assegnandole un punteggio
    ricavato dalla funzione funzione_punteggio passata in input. 
    Somma tutti i punteggi ottenuti per ciascuna categoria delle immagini di train.
    Calcola la media per ogni categoria 
    e ritorna la categoria ritornata dalla funzione funzione_migliore_punteggio passata in input
    
    lista_pixel_test: lista dei valori dei pixel dell'immagine di test
    file_train: path del file da cui ricavare le imamgini di train
    funzione_punteggio: funzione per calcolare il punteggio di somiglianza tra 2 immagini
                        prende in input:
                        lista_pixel_test: lista dei valori dei pixel dell'imamgine di test
                        lista_pixel_train: lista dei valori dei pixel dell'imamgine di train
                        resituisce in output un numero
    funzione_migliore_punteggio: funzione per calacolare la categoria che meglio approssima l'immagine di test
                                 prende in input un dizionario con chiavi le categorie
                                 e valori i rispettivi punteggi 
                                 e ritorna una delle categorie chiavi del dizionario
    
    return: la classe considerata come migliore per classificare l'immagine di test passata in input calcolata come media dei punteggi ragiunti dalle singole classi
    '''
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #inserisco il carattere separatore delle colonne nel csv
    k_car_sep = ','
    #creo il dizionario che conterra i punteggi
    #punteggi = {} 
    punteggi = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
    #creo il dizionario che conterra i contatori di quanti corrispondenze per ogni simbolo si trovano per fare successivamente la media
    #contatori = {}
    contatori = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
    ''''
    #popolo i dizionari
    for simb in k_lista_simboli:
        punteggi[simb] = 0
        contatori[simb] = 0
    '''
    #apro il file di train
    f_train = open(file_train, 'r')
    #scarto la prima riga
    riga_train = f_train.readline()
    riga_train = f_train.readline()
    while riga_train not in ('', '\n'):
        #itero per tutto il file di train
        #cerco il numero rappresentato
        numero_train = riga_train[ : riga_train.find(k_car_sep)]
        #converto in lista la riga di train
        lista_riga_train = [ int(s) for s in riga_train.split(k_car_sep)[1:] ] #escludo il primo carattere perche escludo la prima colonna da tutto il file
        #calcolo il punteggio delle 2 configurazioni
        punteggi[numero_train] += funzione_punteggio(lista_pixel_test, lista_riga_train)
        #incremento il contatore
        contatori[numero_train] += 1
        #leggo la riga successiva
        riga_train = f_train.readline()
    #chiudo il file di train
    f_train.close()
    #calcolo le medie dei punteggi
    for n in k_lista_simboli:
        punteggi[n] /= contatori[n]
    #calcolo la chiave migliore per la lista di punteggi 
    #print('Punteggi: ', punteggi)
    return funzione_migliore_punteggio(punteggi)

def approssimazione_knn_generale(lista_pixel_test, file_train, funzione_distanza, p_raggio, decremento_raggio = 1.):
    '''
    Funzione generale per calcolare la migliore approssimazione data l'immagine di test basandosi su un conto di tipo knn
    La funzione confronta l'immagine di test con ogni immagine di train
    assegnandole un punteggio che rappresenta la distanza tra di esse.
    Tale punteggio e' ricavato dalla funzione funzione_distanza passata in input.
    In seguito si considerano tutte le imamgini di train che distano al massimo p_raggio da quella di test.
    Ritorna la classe che e' presente in percentuale maggiore  basandosi sul numero totale di
    imamgini di train per quella classe.
    Nel caso in cui ci siano 2 o piu' classi con la percentuale di 100%
    Si riesegue il controllo decrementando il raggio del valore passato in input
    fino a quando il raggio non risulterebbe <=0 oppure 
    c'e' al massimo una classe con probabilita di 100%
    
    lista_pixel_test: lista dei valori dei pixel dell'immagine di test
    file_train: path del file da cui ricavare le imamgini di train
    funzione_distanza: funzione per calcolare la distanza tra 2 immagini
                       prende in input:
                       lista_pixel_test: lista dei valori dei pixel dell'imamgine di test
                       lista_pixel_train: lista dei valori dei pixel dell'imamgine di train
                       resituisce in output un numero
    p_raggio: numero che rappresenta il massimo raggio da considerare per l'algoritmo di knn
    decremento_raggio: numero che rappresenta di quanto decrementare il raggio
                       nel caso in cui ci siano piu' classi con probabilita del 100%
    
    return: la classe considerata come migliore per classificare l'immagine di test passata in input calcolata con il metodo di knn
    '''
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #inserisco il carattere separatore delle colonne nel csv
    k_car_sep = ','
    #dimensione dell'immagine in pixel, ovvero il massimo raggio
    k_dim_imm = 28
    #inizializzo il raggio di partenza
    raggio_knn = p_raggio
    #creo il dizionario che conterra le distanze
    raggi = {} 
    #creo il dizionario che conterra i contatori di quanti corrispondenze per ogni simbolo si trovano per fare successivamente percentuale
    #contatori = {}
    contatori = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
    #inizializzo il dizionario che utilizzero in seguito per contare quanti elementi ci sono entro un certo raggio
    #elementi_in_raggio = {}
    elementi_in_raggio = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
    ''''
    #popolo i dizionari
    for simb in k_lista_simboli:
        contatori[simb] = 0
        elementi_in_raggio[simb] = 0
    '''
    #apro il file di train
    f_train = open(file_train, 'r')
    #scarto la prima riga
    riga_train = f_train.readline()
    riga_train = f_train.readline()
    while riga_train not in ('', '\n'):
        #itero per tutto il file di train
        #cerco il numero rappresentato
        numero_train = riga_train[ : riga_train.find(k_car_sep)]
        #converto in lista la riga di train
        lista_riga_train = [ int(s) for s in riga_train.split(k_car_sep)[1:] ] #escludo il primo carattere perche escludo la prima colonna da tutto il file
        #calcolo il punteggio delle 2 configurazioni
        punteggio = funzione_distanza(lista_pixel_test, lista_riga_train)
        #print('PUNTEGGIO:', punteggio)
        #aggiungo al dizionario dei raggi solo se minore del raggio limite
        if punteggio <= raggio_knn:
            #controllo se il punteggio non e stato censito e quindi lo inseriso
            if punteggio not in raggi.keys():
                raggi[punteggio] = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
            #incremento il numero di casi per il relativo punteggio e la classe di immagine train
            raggi[punteggio][numero_train] += 1
        #incremento il contatore
        contatori[numero_train] += 1
        #leggo la riga successiva
        riga_train = f_train.readline()
    #chiudo il file di train
    f_train.close()
    #calcolo gli elementi in raggio
    for k in raggi.keys():
        #vado a sommare solo quelli <= del raggio dato
        #if k <= raggio_knn:
        in_corona = raggi[k]
        #per il determinato raggio sommo tutti gli elementi che compaiono
        for simb in in_corona.keys():
            elementi_in_raggio[simb] += in_corona[simb]
    #ora elementi_in_raggio ha per ogni chiave il numero di elementi di quel tipo entro il raggio raggio_knn
    elemento_ideale = trova_elemento_piu_percentuale(elementi_in_raggio, contatori)
    #print('massimo raggio: ', max(list(raggi.keys() ) ), 'minimo raggio: ', min(list(raggi.keys() ) ))
    #inizializzo una lista dei raggi ordinataal contrario dal piu grande
    if elemento_ideale == '-1':
        lista_raggi = sorted(list(raggi.keys()))[::-1]
    #se elemento_idela e '-1' significa che ci sono 2 chiavi con massima percentuale
    while elemento_ideale == '-1' and (raggio_knn - decremento_raggio)>0:
        #riduco il raggio e tolgo da elementi_in_raggio i rispettivi numeri
        '''
        #calcolo la corona
        in_corona = raggi[raggio_knn]
        #tolgo gli elementi dal elementi_in_raggio
        for k in elementi_in_raggio.keys():
            elementi_in_raggio[k] -= in_corona[k]
        #ricalcolo il migliore elemento
        elemento_ideale = trova_elemento_piu_percentuale(elementi_in_raggio, contatori)
        '''
        #cerco gli elementi nei raggi tra raggio_knn e raggio_knn-decremento_raggio
        for r in lista_raggi:
            if (raggio_knn - decremento_raggio) < r <= raggio_knn:
                corona = raggi[r]
                for k in elementi_in_raggio.keys():
                    elementi_in_raggio[k] -= corona[k]
            #poiche lista_raggi ordinata, se trovo un elemento piu piccolo del futuro raggio interrompo il ciclo
            elif r <= (raggio_knn - decremento_raggio):
                break
        #print(elementi_in_raggio)
        #ricalcolo il migliore elemento
        elemento_ideale = trova_elemento_piu_percentuale(elementi_in_raggio, contatori)
        #riduco il raggio knn
        raggio_knn -= decremento_raggio
    #se elemento ideale e -1 significa che il rqaggio richia di diventare negativo, 
    #richiamo la funzione trova_elemento_piu_percentuale con flag ultimo_passaggio=True
    if elemento_ideale == '-1':
        elemento_ideale = trova_elemento_piu_percentuale(elementi_in_raggio, contatori, True)
    #ritorno l'elemento ideale calcolato
    return elemento_ideale

def approssimazione_media_colorazioni_generale(lista_pixel_test, file_train, funzione_somiglianza_immagini, funzione_migliore_punteggio, flag_autoapprendimento = False):
    '''
    Funzione che basa la classificazione su una matrice 
    che contiene la media delle colorazioni di tutte le matrici
    di una determinata categoria
    
    lista_pixel_test: lista dei valori dei pixel dell'immagine di test
    file_train: path del file da cui ricavare le imamgini di train
    funzione_somiglianza_immagini: funzione per calcolare il punteggio di somiglianza tra 2 immagini
                                   prende in input:
                                   lista_pixel_test: lista dei valori dei pixel dell'imamgine di test
                                   lista_pixel_train: lista dei valori dei pixel dell'imamgine di train
                                   resituisce in output un numero
    funzione_migliore_punteggio: funzione per calacolare la categoria che meglio approssima l'immagine di test
                                 prende in input un dizionario con chiavi le categorie
                                 e valori i rispettivi punteggi 
                                 e ritorna una delle categorie chiavi del dizionario
    flag_autoapprendimento: Booleano che indica se si desira l'autoapprendimento
                            in caso il flag sia False si prende sempre come insieme di train lo stesso file
    
    return: la classe considerata come migliore per classificare l'immagine di test passata in input
    ''' 
    def calcola_media_colorazione(p_file_train, flag_autoapprendimento):
        '''
        Funzione per creare la matrice media delle colorazioni ed eventualmente il file associato
        
        p_file_train: path del file di train da leggere per ricavare le imamgini di train
        flag_autoapprendimento: Booleano che indica se e necessaario riaprire il file di train
                                oppure ci si pua' basare sul file precedentemente prodotto
        
        return: dizionario che ha come valori le matrici medie
        '''
        #inizializzo il nome del file della media
        k_nome_file = 'media_valori_pixel_immagini'
        #verifico se il file non e presente oppure il flag di autoapprendimento e true
        if (flag_autoapprendimento == True) or (k_nome_file not in os.listdir()):
            #inizializzo la dimensione dell'immagine
            k_dimm_imm = 28
            #creo la lista dei simboli da utilizzare
            k_lista_simboli = [str(i) for i in range(10)]
            #inserisco il carattere separatore delle colonne nel csv
            k_car_sep = ','
            #inizializzo il dizionario che conterra i contatori del numero di immagini
            contatori = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
            #inzializzo le matrici associate in cui considerero la media
            matrici_classi = dict( zip( k_lista_simboli, [np.zeros(k_dimm_imm**2)]*len(k_lista_simboli) ) )
            #print('matrici_classi: ', matrici_classi)
            #apro il file di train
            file_train = open(p_file_train, 'r')
            #scarto la prima riga
            riga_train = file_train.readline()
            riga_train = file_train.readline()
            #itero fino a quando il file non e finito
            while riga_train not in ('', '\n'):
                #cerco il numero rappresentato
                numero_train = riga_train[ : riga_train.find(k_car_sep)]
                #converto in lista la riga di train
                lista_riga_train = [ int(s) for s in riga_train.split(k_car_sep)[1:] ] #escludo il primo carattere perche escludo la prima colonna da tutto il file
                #eseguo la somma alla matrice totale
                matrici_classi[numero_train] += np.array(lista_riga_train)
                #incremento il contatore
                contatori[numero_train] += 1
                #leggo la riga successiva
                riga_train = file_train.readline()
            #chiudo il file 
            file_train.close()
            #calcolo la media di ciascuna matrice e converto il valore in lista
            for k in matrici_classi.keys():
                matrici_classi[k] = list( matrici_classi[k]/contatori[k] )
            #se arrivo qui significa che matrici classi conterra le matrici medie per ogni classe
            #se il flag di autoapprendimento e false, significa che devo creare il file
            
            if flag_autoapprendimento == False:
                #creo il file
                open(k_nome_file, 'w').write(str(matrici_classi))
            #ritorno il dizionario calcolato
            return matrici_classi
        else:
            #il file esiste, lo leggo e ritorno il valore contenuto
            riga_file_matrice_media = open(k_nome_file, 'r').read()
            #ritorno il dizionario presente nel file
            return eval(riga_file_matrice_media)
        
    #calcolo le imamgini medie con la funzione calcola_media_colorazione
    immagini_medie = calcola_media_colorazione(file_train, flag_autoapprendimento)
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #creo un dizionario con i rispettivi punteggi
    punteggi = {}
    #calcolo i rispettivi punteggi
    for k in immagini_medie.keys():
        punteggi[k] = funzione_somiglianza_immagini(lista_pixel_test, immagini_medie[k])
    #attraverso la funzione dei punteggi calcolo il migliore
    return funzione_migliore_punteggio(punteggi)
    
def approssimazione_media_n_px_col_generale(lista_pixel_test, file_train, funzione_somiglianza_immagini, funzione_migliore_punteggio, soglia, flag_autoapprendimento = False):
    '''
    Funzione che basa la classificazione su una matrice 
    che contiene la media del numero di pixel colorati di tutte le matrici
    di una determinata categoria
    
    lista_pixel_test: lista dei valori dei pixel dell'immagine di test
    file_train: path del file da cui ricavare le imamgini di train
    funzione_somiglianza_immagini: funzione per calcolare il punteggio di somiglianza tra 2 immagini
                                   prende in input:
                                   lista_pixel_test: lista dei valori dei pixel dell'imamgine di test
                                   lista_pixel_train: lista dei valori dei pixel dell'imamgine di train
                                   resituisce in output un numero
    funzione_migliore_punteggio: funzione per calacolare la categoria che meglio approssima l'immagine di test
                                 prende in input un dizionario con chiavi le categorie
                                 e valori i rispettivi punteggi 
                                 e ritorna una delle categorie chiavi del dizionario
    soglia: valore per cui considerare un pixel colorato o meno
    flag_autoapprendimento: Booleano che indica se si desira l'autoapprendimento
                            in caso il flag sia False si prende sempre come insieme di train lo stesso file
    
    return: la classe considerata come migliore per classificare l'immagine di test passata in input
    ''' 
    def calcola_media_n_px_col(p_file_train, flag_autoapprendimento, soglia):
        '''
        Funzione per creare la matrice media del numero medio di pixel colorati ed eventualmente il file associato
        
        p_file_train: path del file di train da leggere per ricavare le imamgini di train
        flag_autoapprendimento: Booleano che indica se e necessaario riaprire il file di train
                                oppure ci si pua' basare sul file precedentemente prodotto
        soglia: valore per cui considerare un pixel colorato o meno
        
        return: dizionario che ha come valori le matrici medie
        '''
        #inizializzo il nome del file della media
        k_nome_file = 'media_n_pixel_col_immagini_%d'%(soglia)
        #verifico se il file non e presente oppure il flag di autoapprendimento e true
        if (flag_autoapprendimento == True) or (k_nome_file not in os.listdir()):
            #inizializzo la dimensione dell'immagine
            k_dimm_imm = 28
            #creo la lista dei simboli da utilizzare
            k_lista_simboli = [str(i) for i in range(10)]
            #inserisco il carattere separatore delle colonne nel csv
            k_car_sep = ','
            #inizializzo il dizionario che conterra i contatori del numero di immagini
            contatori = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
            #inzializzo le matrici associate in cui considerero la media
            matrici_classi = dict( zip( k_lista_simboli, [np.zeros(k_dimm_imm**2)]*len(k_lista_simboli) ) )
            #print('matrici_classi: ', matrici_classi)
            #apro il file di train
            file_train = open(p_file_train, 'r')
            #scarto la prima riga
            riga_train = file_train.readline()
            riga_train = file_train.readline()
            #itero fino a quando il file non e finito
            while riga_train not in ('', '\n'):
                #cerco il numero rappresentato
                numero_train = riga_train[ : riga_train.find(k_car_sep)]
                #converto in lista la riga di train
                lista_riga_train = [ int(s) for s in riga_train.split(k_car_sep)[1:] ] #escludo il primo carattere perche escludo la prima colonna da tutto il file
                #eseguo la somma alla matrice totale
                matrici_classi[numero_train] += (np.array(lista_riga_train) >= soglia)
                #incremento il contatore
                contatori[numero_train] += 1
                #leggo la riga successiva
                riga_train = file_train.readline()
            #chiudo il file 
            file_train.close()
            #calcolo la media di ciascuna matrice e converto il valore in lista
            for k in matrici_classi.keys():
                matrici_classi[k] = list( matrici_classi[k]/contatori[k] )
            #se arrivo qui significa che matrici classi conterra le matrici medie per ogni classe
            #se il flag di autoapprendimento e false, significa che devo creare il file
            
            if flag_autoapprendimento == False:
                #creo il file
                open(k_nome_file, 'w').write(str(matrici_classi))
            #ritorno il dizionario calcolato
            return matrici_classi
        else:
            #il file esiste, lo leggo e ritorno il valore contenuto
            riga_file_matrice_n_col = open(k_nome_file, 'r').read()
            #ritorno il dizionario presente nel file
            return eval(riga_file_matrice_n_col)
        
    #calcolo le imamgini medie con la funzione calcola_media_colorazione
    immagini_medie = calcola_media_n_px_col(file_train, flag_autoapprendimento, soglia)
    #creo un dizionario con i rispettivi punteggi
    punteggi = {}
    #calcolo i rispettivi punteggi
    for k in immagini_medie.keys():
        punteggi[k] = funzione_somiglianza_immagini(lista_pixel_test, immagini_medie[k])
    #attraverso la funzione dei punteggi calcolo il migliore
    return funzione_migliore_punteggio(punteggi)


def approssimazione_knn_val_assoluto_generale(lista_pixel_test, file_train, funzione_distanza, p_raggio):
    '''
    Funzione generale per calcolare la migliore approssimazione data l'immagine di test basandosi su un conto di tipo knn
    La funzione confronta l'immagine di test con ogni immagine di train
    assegnandole un punteggio che rappresenta la distanza tra di esse.
    Tale punteggio e' ricavato dalla funzione funzione_distanza passata in input.
    In seguito si considerano tutte le imamgini di train che distano al massimo p_raggio da quella di test.
    Ritorna la classe che e' presente in numero maggiore  
    
    lista_pixel_test: lista dei valori dei pixel dell'immagine di test
    file_train: path del file da cui ricavare le imamgini di train
    funzione_distanza: funzione per calcolare la distanza tra 2 immagini
                       prende in input:
                       lista_pixel_test: lista dei valori dei pixel dell'imamgine di test
                       lista_pixel_train: lista dei valori dei pixel dell'imamgine di train
                       resituisce in output un numero
    p_raggio: numero che rappresenta il massimo raggio da considerare per l'algoritmo di knn
    
    return: la classe considerata come migliore per classificare l'immagine di test passata in input calcolata con il metodo di knn
    '''
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #inserisco il carattere separatore delle colonne nel csv
    k_car_sep = ','
    #dimensione dell'immagine in pixel, ovvero il massimo raggio
    k_dim_imm = 28
    #inizializzo il raggio di partenza
    raggio_knn = p_raggio
    #creo il dizionario che conterra le distanze
    raggi = {} 
    #creo il dizionario che conterra i contatori di quanti corrispondenze per ogni simbolo si trovano per fare successivamente percentuale
    #contatori = {}
    #contatori = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
    #inizializzo il dizionario che utilizzero in seguito per contare quanti elementi ci sono entro un certo raggio
    #elementi_in_raggio = {}
    elementi_in_raggio = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
    ''''
    #popolo i dizionari
    for simb in k_lista_simboli:
        contatori[simb] = 0
        elementi_in_raggio[simb] = 0
    '''
    #apro il file di train
    f_train = open(file_train, 'r')
    #scarto la prima riga
    riga_train = f_train.readline()
    riga_train = f_train.readline()
    while riga_train not in ('', '\n'):
        #itero per tutto il file di train
        #cerco il numero rappresentato
        numero_train = riga_train[ : riga_train.find(k_car_sep)]
        #converto in lista la riga di train
        lista_riga_train = [ int(s) for s in riga_train.split(k_car_sep)[1:] ] #escludo il primo carattere perche escludo la prima colonna da tutto il file
        #calcolo il punteggio delle 2 configurazioni
        punteggio = funzione_distanza(lista_pixel_test, lista_riga_train)
        #print('PUNTEGGIO:', punteggio)
        #aggiungo al dizionario dei raggi solo se minore del raggio limite
        if punteggio <= raggio_knn:
            #controllo se il punteggio non e stato censito e quindi lo inseriso
            if punteggio not in raggi.keys():
                raggi[punteggio] = dict( zip( k_lista_simboli, [0]*len(k_lista_simboli) ) )
            #incremento il numero di casi per il relativo punteggio e la classe di immagine train
            raggi[punteggio][numero_train] += 1
        #incremento il contatore
        #contatori[numero_train] += 1
        #leggo la riga successiva
        riga_train = f_train.readline()
    #chiudo il file di train
    f_train.close()
    #calcolo gli elementi in raggio
    for k in raggi.keys():
        #vado a sommare solo quelli <= del raggio dato
        #if k <= raggio_knn:
        in_corona = raggi[k]
        #per il determinato raggio sommo tutti gli elementi che compaiono
        for simb in in_corona.keys():
            elementi_in_raggio[simb] += in_corona[simb]
    print()
    #ora elementi_in_raggio ha per ogni chiave il numero di elementi di quel tipo entro il raggio raggio_knn
    #print('massimo raggio: ', max(list(raggi.keys() ) ), 'minimo raggio: ', min(list(raggi.keys() ) ))
    elemento_ideale = punteggio_migliore_massimo(elementi_in_raggio)
    return elemento_ideale


        
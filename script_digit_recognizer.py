'''
script per la suddivisione delle immagini manoscritte di numeri
Per ogni immagine di test verra' confrontata con ogni immagine di train e verra' assocviato un punteggio
Si sperimenteranno diverse tecniche di assegnamento dei punteggi
'''

import os, math
from PIL import Image as img
import numpy as np
from shutil import copyfile

#moduli locali
import funzioni_somiglianza as fs
import funzioni_approssimazione as fa

def crea_immagine(cartella, numero_imm, lista_pixel):
    '''
    Funzione per creare all'interno della cartella passata in input
    l'immagine cosrrispondente alla lista di pixel passata in input
    
    cartella: stringa che rappresenta il path della cartella 
    numero_imm: numero che rappresenta il nome dell'immagine
    lista_pixel: lista di interi che rappresentano i valori dei pixel dell'immagine

    return: None
    '''
    k_dim_imm_orizz = 28
    #suddivido la lista di pixel in righe
    #ipotizzo che l'immagine sia quadrata
    pixels = [ lista_pixel[ i*k_dim_imm_orizz: (i+1)*k_dim_imm_orizz ] for i in range(k_dim_imm_orizz) ]
    #converto i pixel in un array
    array = np.array(pixels, dtype=np.uint8)
    #Utilizzo PIL per creare un'immagine dall'array di pixel
    new_image = img.fromarray(array)
    #salvo l'immagine nella destinazione
    new_image.save(os.path.join(cartella, '%d.png'%(numero_imm)))
    new_image.close()
    
def crea_immagini_train(cartella_dest, file_train):
    '''
    Funzione per creare la rappresentazioen di tutte le imamgini del file train
    
    cartella:dest: path che rappresenta la cartella dove inserire le imamgini train
    file_train: path del file train da aprire

    return: None
    '''
    #verifico esistenza cartella destinazione
    if not os.path.exists(cartella_dest):
        os.mkdir(cartella_dest)    
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #creo le cartelle dei file di destinazione
    for n in k_lista_simboli:
        path_cartella = os.path.join(cartella_dest, n)
        if not os.path.exists(path_cartella):
            os.mkdir(path_cartella)
    k_car_sep = ',' #carattere separatore nel file csv
    #apro il file di train
    f_train = open(file_train, 'r')
    #scarto la prima riga
    riga_train = f_train.readline()
    riga_train = f_train.readline()
    #creo un indice che rappresenta il numero dell'immagine train
    n_immagine_train = 0
    while riga_train not in ('', '\n'):
        #itero fino alla fine del file
        #prendo il numero rappresentato
        numero_train = riga_train[ : riga_train.find(k_car_sep)]
        #converto in lista la riga di train
        lista_riga_train = [ int(s) for s in riga_train.split(k_car_sep)[1:] ] #escludo il primo carattere perche escludo la prima colonna da tutto il file
        crea_immagine(os.path.join(cartella_dest, numero_train), n_immagine_train, lista_riga_train)
        #leggo la riga successiva
        n_immagine_train += 1
        riga_train = f_train.readline()
    #chiudo il file
    f_train.close()

def crea_immagini_test(cartella_dest, file_test):
    '''
    Funzione per creare la rappresentazioen di tutte le imamgini del file test
    
    cartella:dest: path che rappresenta la cartella dove inserire le imamgini test
    file_test: path del file test da aprire

    return: None
    '''
    #verifico esistenza cartella destinazione
    if not os.path.exists(cartella_dest):
        os.mkdir(cartella_dest)    
    k_car_sep = ',' #carattere separatore nel file csv
    #apro il file di train
    f_test = open(file_test, 'r')
    #scarto la prima riga
    riga_test = f_test.readline()
    riga_test = f_test.readline()
    #creo un indice che rappresenta il numero dell'immagine train
    n_immagine_test = 1
    while riga_test not in ('', '\n'):
        #itero fino alla fine del file
        #converto in lista la riga di train
        lista_riga_test = [ int(s) for s in riga_test.split(k_car_sep) ]
        crea_immagine(cartella_dest, n_immagine_test, lista_riga_test)
        #leggo la riga successiva
        n_immagine_test += 1
        riga_test = f_test.readline()
    #chiudo il file
    f_test.close()

    
def suddividi_immagini_generale(p_file_test, p_file_train, p_cartella_dest, p_funzione_miglior_abbinamento, p_num_righe_da_saltare=0 ):
    '''
    Funzione per suddividere le immagini in categorie caso generale
    
    p_file_test: path del file contenente i test da provare
    p_file_train: path del file contenente i file di training
    p_cartella_dest: cartella di destinazione nella quale inserire la classificazione
    p_funzione_miglior_abbinamento: funzione che prende in input la lista di pixel 
        dell'immagine di test, il nome del file della lista di train
        e ritorna il migliore abbinamento calcolato per l'immagine di test

    return: None
    '''
    print('cartella destinazione: ', p_cartella_dest)
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #inserisco il carattere separatore delle colonne nel csv
    k_car_sep = ','
    #verifico esistenza cartella destinazione
    if not os.path.exists(p_cartella_dest):
        os.mkdir(p_cartella_dest)
    #creo le cartelle dei file di destinazione
    for n in k_lista_simboli:
        path_cartella = os.path.join(p_cartella_dest, n)
        if not os.path.exists(path_cartella):
            os.mkdir(path_cartella) 
    #per evitare occupazione inutili di memoria leggo le immagini direttamente dai file
    f_test = open( p_file_test, 'r' )
    #escludo la prima riga
    riga_test = f_test.readline()
    riga_test = f_test.readline()
    #creo un id della riga di test da utilizzare in seguito per nominare i file
    n_riga_test = 1
    #escludo tante righe quanto mi viene passato in input
    for n_riga_salta in range(p_num_righe_da_saltare):
        riga_test = f_test.readline()
        n_riga_test += 1
    while riga_test not in ('', '\n'):
        #ciclo fino alla fine del file o quando incontro una riga vuota
        #converto in lista la riga di test
        lista_riga_test = [ int(s) for s in riga_test.split(k_car_sep) ]
        #calcolo il migliore abbinamento con la funzione passata in input
        numero_migliore = p_funzione_miglior_abbinamento(lista_riga_test, p_file_train)
        #creo l'immagine 
        crea_immagine(os.path.join(p_cartella_dest, numero_migliore), n_riga_test, lista_riga_test)
        #leggo la prossima riga del file di test
        print('Immagine numero %d inserita nella cartella %s'%(n_riga_test, numero_migliore))
        n_riga_test += 1
        riga_test = f_test.readline()
    f_test.close()

def suddividi_immagini_generale_autoapprendimento(p_file_test, p_file_train, p_cartella_dest, p_funzione_miglior_abbinamento, p_num_righe_da_saltare=0 ):
    '''
    Funzione per suddividere le immagini in categorie caso generale
    Implementa anche una procedura di autoapprendimento dai test che classifica
    
    p_file_test: path del file contenente i test da provare
    p_file_train: path del file contenente i file di training
    p_cartella_dest: cartella di destinazione nella quale inserire la classificazione
    p_funzione_miglior_abbinamento: funzione che prende in input la lista di pixel 
        dell'immagine di test, il nome del file della lista di train
        e ritorna il migliore abbinamento calcolato per l'immagine di test
    
    return: None
    '''
    print('cartella destinazione: ', p_cartella_dest)
    #creo la lista dei simboli da utilizzare
    k_lista_simboli = [str(i) for i in range(10)]
    #inserisco il carattere separatore delle colonne nel csv
    k_car_sep = ','
    #verifico esistenza cartella destinazione
    if not os.path.exists(p_cartella_dest):
        os.mkdir(p_cartella_dest)
    #creo le cartelle dei file di destinazione
    for n in k_lista_simboli:
        path_cartella = os.path.join(p_cartella_dest, n)
        if not os.path.exists(path_cartella):
            os.mkdir(path_cartella) 
    #per evitare occupazione inutili di memoria leggo le immagini direttamente dai file
    f_test = open( p_file_test, 'r' )
    #escludo la prima riga
    riga_test = f_test.readline()
    riga_test = f_test.readline()
    #creo un id della riga di test da utilizzare in seguito per nominare i file
    n_riga_test = 1
    #escludo tante righe quanto mi viene passato in input
    for n_riga_salta in range(p_num_righe_da_saltare):
        riga_test = f_test.readline()
        n_riga_test += 1
    #copio il file nella cartella di destinazione
    file_autoapprend = os.path.join(p_cartella_dest, 'train_autoappren.csv')
    #controllo che il file non sia lo stesso di train
    if file_autoapprend != p_file_train:
        #se sono diversi copio il file
        copyfile(p_file_train, file_autoapprend)
    while riga_test not in ('', '\n'):
        #ciclo fino alla fine del file o quando incontro una riga vuota
        #converto in lista la riga di test
        lista_riga_test = [ int(s) for s in riga_test.split(k_car_sep) ]
        #calcolo il migliore abbinamento con la funzione passata in input
        numero_migliore = p_funzione_miglior_abbinamento(lista_riga_test, file_autoapprend)
        #creo l'immagine 
        crea_immagine(os.path.join(p_cartella_dest, numero_migliore), n_riga_test, lista_riga_test)
        #inserisco la nuova riga nel file csv di autoapprendimento
        fl_autoappr = open(file_autoapprend, 'a')
        fl_autoappr.write(numero_migliore + k_car_sep + riga_test) #inserisco senza l'\n perche compreso in readline
        fl_autoappr.close()
        #leggo la prossima riga del file di test
        print('Immagine numero %d inserita nella cartella %s'%(n_riga_test, numero_migliore))
        n_riga_test += 1
        riga_test = f_test.readline()
    f_test.close()


'''
comandi utili per combinare le funzioni di approssimazione con quelle di somiglianza



Funzioni di suddivisione generale:

suddividi_immagini_generale( p_file_test = os.path.join('digit-recognizer', 'test.csv'), 
                             p_file_train = os.path.join('digit-recognizer', 'train.csv'), 
                             p_cartella_dest = 'suddivisione_[...]', 
                             p_funzione_miglior_abbinamento = 
                                 [...]
                             p_num_righe_da_saltare=0 
                             )
                             
suddividi_immagini_generale_autoapprendimento( p_file_test = os.path.join('digit-recognizer', 'test.csv'), 
                                               p_file_train = os.path.join('digit-recognizer', 'train.csv'), 
                                               p_cartella_dest = 'suddivisione_', 
                                               p_funzione_miglior_abbinamento = 
                                                   [...]
                                               p_num_righe_da_saltare=0 
                             )





Funzioni di approssimazione (gia in forma con lambda):

lambda x,y : fa.approssimazione_media_generale(
    lista_pixel_test = x, 
    file_train = y, 
    funzione_punteggio = 
        [...], 
    funzione_migliore_punteggio = 
        fa.punteggio_migliore_[...]
)

lambda x,y : fa.approssimazione_knn_generale(
    lista_pixel_test = x, 
    file_train = y, 
    funzione_distanza = 
        [...], 
    p_raggio = [...],
    decremento_raggio = [...]
)

lambda x,y : fa.approssimazione_media_colorazioni_generale(
    lista_pixel_test = x, 
    file_train = y, 
    funzione_somiglianza_immagini = 
        [...], 
    funzione_migliore_punteggio = 
        fa.punteggio_migliore_[...],
    flag_autoapprendimento = [...]
)

lambda x,y : fa.approssimazione_media_n_px_col_generale(
    lista_pixel_test = x, 
    file_train = y, 
    funzione_somiglianza_immagini = 
        [...], 
    funzione_migliore_punteggio = 
        fa.punteggio_migliore_[...],
    soglia = [...]
    flag_autoapprendimento = [...]
)

lambda x,y : fa.approssimazione_knn_val_assoluto_generale(
    lista_pixel_test = x, 
    file_train = y, 
    funzione_distanza = 
        [...], 
    p_raggio = [...]
)



Funzioni di somiglianza (gia nell'eventuale formato lambda)

lambda a,b : fs.punteggio_intorni_train(
    lista_pixel_test = a, 
    lista_pixel_train = b, 
    solo_neri = [...]
)

lambda a,b : fs.punteggio_intorni_test(
    lista_pixel_test = a, 
    lista_pixel_train = b, 
    solo_neri = [...]
)

lambda a,b : fs.punteggio_quadrettatura(
    lista_pixel_test = a, 
    lista_pixel_train = b, 
    pixel_lato = [...],
    soglia = [...]
)

lambda a,b : fs.punteggio_categoria_media_n_px_col(
    lista_pixel_test = a, 
    lista_pixel_train = b, 
    soglia = [...]
)

lambda a,b : fs.punteggio_minimo_matriciale(
    lista_pixel_test = a, 
    lista_pixel_train = b, 
    funzione_confronto_matrici = 
        [...],
    traslazione = [...],
    soglia = [...]
)




Funzioni da passare come argomento alla funzione_confronto_matrici (eventualmente in formato lambda)

fs.sum_diff_pixel

lambda m1, m2: fs.num_pixel_diversi(
    matrice1 = m1,
    matrice2 = m2,
    soglia = [...]
)

lambda m1, m2: fs.diff_quadrett_px_col_perc(
    matrice1 = m1,
    matrice2 = m2,
    soglia = [...],
    lato_quadrato = [...]
)

ambda m1, m2: fs.diff_quadrett_inchiostro_perc(
    matrice1 = m1,
    matrice2 = m2,
    lato_quadrato = [...]
)

fs.confr_perc_px_px

fs.distanza_euclidea
'''

'''
Considerazioni sulla composizione di funzioni

Il metodo della media combinato con la funzione intorni_train e solo_neri = False presenta una precisione inferiore rispetto al rispettivo knn 
La funzione punteggio_quadrettatura sembra si comporti meglio in associazione con media_generale piuttosto che con knn_generale
La combinazione della funzione di traslazione con sum_diff_pixel risulta meno precisa se applicato insieme all'algoritmo knn 
    che a quello della media (il quale risulta risulta pero' molto piu' lento)
I metodi piu' imprecisi sono quelli che si basano sulla media di tutte le matrici per ogni categoria
La combinazione piu' precisa fino ad ora testata (livello di precisione di oltre 90% du un campione di 1800 classificazioni) risulta essere:
    metodo di approssimazione knn_generale (raggio di 0.7 e decremento raggio di 0.05), punteggio_minimo_matriciale senza traslazione e confr_perc_px_px

'''

if __name__ == '__main__':
    #suddividi_immagini(os.path.join('digit-recognizer', 'test.csv'), os.path.join('digit-recognizer', 'train.csv'), 'prova_suddivisione_1', lambda x, y: punteggio_intorni(x, y, False), punteggio_migliore_minimo)
    
    print('INIZIO')
    
    #creo tutte le immagini di train
    '''
    crea_immagini_train('immagini_train', 
                        os.path.join('digit-recognizer', 
                        'train.csv')
                       )
    '''
    
    #creo tutte le imamgini di test
    '''
    crea_immagini_test(cartella_dest = 'immagini_test', 
                       file_test= os.path.join('digit-recognizer', 
                        'test.csv')
                       )
    '''
    
    #suddivido le immagini attraverso il metodo del knn e funzione di somiglianza traslazione + confr_perc_px_px 
    #precisione quasi 90%
    
    suddividi_immagini_generale_autoapprendimento(p_file_test = os.path.join('digit-recognizer', 'test.csv'), 
                                p_file_train = os.path.join('suddivisione_auto_knn_no_traslazione_confrpercpxpx_r07', 'train_autoappren_old.csv'), 
                                p_cartella_dest = 'suddivisione_auto_knn_no_traslazione_confrpercpxpx_r07', 
                                p_funzione_miglior_abbinamento = 
                                    lambda x, y: fa.approssimazione_knn_generale(lista_pixel_test = x, 
                                                                                 file_train = y, 
                                                                                 funzione_distanza = 
                                                                                     lambda a,b : fs.punteggio_minimo_matriciale(
                                                                                         lista_pixel_test = a,
                                                                                         lista_pixel_train = b,
                                                                                         funzione_confronto_matrici = 
                                                                                             fs.confr_perc_px_px,
                                                                                         traslazione = False,
                                                                                         soglia = 0
                                                                                     ) ,
                                                                                 p_raggio = 0.7,
                                                                                 decremento_raggio = 0.05
                                                                                ),
                               p_num_righe_da_saltare = 1891
                               )
    #arrivato fino a 1891
    
    print('FINITO')
    
